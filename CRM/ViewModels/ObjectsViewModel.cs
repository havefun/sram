﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM
{
    class ObjectsViewModel
    {
        public List<BuildingObject> Objects
        {
            get { return Controller.Objects.ToList(); }
        }
    }
}
