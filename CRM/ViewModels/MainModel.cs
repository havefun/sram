﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace CRM
{
    public class MainModel : INotifyPropertyChanged
    {
        private Order order = new Order();
        private string nameSearch = "";
        private int? minimalWorth;
        private List<Order> orders = Controller.Orders;
        private int? maximalWorth;
        private DateTime? minTime;
        private DateTime? maxTime;
        private bool onlyMine;
        private UserControl content = new Views.MainMenuView();
        private List<UserControl> controls = new List<UserControl>();
        private bool fromStack = false;
        private Client currentClient;
        private BuildingObject currentObject { get; set; }
        private Dictionary<string, string> filters = new Dictionary<string, string>
        {
            {"name", ""},
            {"phone", ""},
            {"email", ""},
            {"complex", ""}
        };
        public string ComplexFilter
        {
            get { return filters["complex"]; }
            set { SetClientFilter("complex", value); }
        }
        public string EmailFilter
        {
            get { return filters["email"]; }
            set { SetClientFilter("email", value); }
        }
        public string PhoneFilter
        {
            get { return filters["phone"]; }
            set { SetClientFilter("phone", value); }
        }
        public string NameFilter
        {
            get { return filters["name"]; }
            set { SetClientFilter("name", value); }
        }
        public Visibility BackElementVisibility
        {
            get 
            { 
                if (controls.Count == 0) return Visibility.Hidden;
                return Visibility.Visible;
            }
        }
        public BuildingObject CurrentObject
        {
            get { return currentObject; }
            set
            {
                currentObject = value;
                OnPropertyChanged("CurrentObject");
            }
        }
        private Order currentOrder;
        public Order CurrentOrder
        {
            get { return currentOrder; }
            set
            {
                currentOrder = value;
                OnPropertyChanged("CurrentOrder");
            }
        }
        public Visibility IsNew
        {
            get { return CurrentOrder.Status == OrderStatus.Новый ? Visibility.Visible : Visibility.Hidden; }
        }
        public Visibility IsSelected
        {
            get { return CurrentOrder.Status == OrderStatus.Осмотр ? Visibility.Visible : Visibility.Hidden; }
        }
        public Visibility IsBought
        {
            get { return CurrentOrder.Status == OrderStatus.Подтвержден ? Visibility.Visible : Visibility.Hidden; }
        }
        public Visibility Finished
        {
            get { return (CurrentOrder.Status == OrderStatus.Завершен || CurrentOrder.Status == OrderStatus.Отклонен) ? Visibility.Visible : Visibility.Hidden; }
        }
        public List<BuildingObject> Objects
        {
            get { return Controller.Objects.Where(q => q.Status == BuildingObjectStatus.Продается).ToList(); }
        }
        public void UpdateOrder()
        {
            if (Root != null)
                Root.UpdateOrder();
            OnPropertyChanged("Clients");
            OnPropertyChanged("CurrentOrder");
            OnPropertyChanged("CurrentOrders");
            OnPropertyChanged("CurrentClient");
            OnPropertyChanged("CurrentObject");
            OnPropertyChanged("MyOrders");
            OnPropertyChanged("MyClients");
            OnPropertyChanged("Objects");

            OnPropertyChanged("IsNew");
            OnPropertyChanged("IsSelected");
            OnPropertyChanged("IsBought");
            OnPropertyChanged("Finished");
        }
        public List<Order> MyOrders
        {
            get
            {
                return Controller.Orders.Where(q => q.Seller.Id == Controller.CurrentUser.Id).Distinct().ToList();
            }
        }
        public void SetClientFilter(string filter, string value)
        {
            if (filters.ContainsKey(filter))
            {
                filters[filter] = value;
                OnPropertyChanged("Clients");
                OnPropertyChanged(char.ToUpper(filter[0]) + filter.Substring(1) + "Filter");
            }
        }
        public void ResetClientFilters()
        {
            var keys =  new List<string>(filters.Keys);
            foreach (var item in keys)
            {
                SetClientFilter(item, "");
            }
        }
        public List<Client> Clients
        {
            get
            {
                if (filters.Count(q => !string.IsNullOrEmpty(q.Value)) == 0)
                    return Controller.Clients;
                return Controller.Clients.Where(q =>               
                    (((q.Name ?? "").IndexOf(filters["complex"], StringComparison.InvariantCultureIgnoreCase) >= 0 && !string.IsNullOrEmpty(filters["complex"]))
                    || ((q.Phone ?? "").IndexOf(filters["complex"], StringComparison.InvariantCultureIgnoreCase) >= 0 && !string.IsNullOrEmpty(filters["complex"]))
                    || ((q.Email ?? "").IndexOf(filters["complex"], StringComparison.InvariantCultureIgnoreCase) >= 0  && !string.IsNullOrEmpty(filters["complex"]))
                    || (q.Id.ToString().IndexOf(filters["complex"], StringComparison.InvariantCultureIgnoreCase) >= 0 && !string.IsNullOrEmpty(filters["complex"])))
                    && ((q.Name ?? "").IndexOf(filters["name"], StringComparison.InvariantCultureIgnoreCase) >= 0 || string.IsNullOrEmpty(filters["name"]))
                    && ((q.Phone ?? "").IndexOf(filters["phone"], StringComparison.InvariantCultureIgnoreCase) >= 0 || string.IsNullOrEmpty(filters["phone"]))
                    && ((q.Email ?? "").IndexOf(filters["email"], StringComparison.InvariantCultureIgnoreCase) >= 0 || string.IsNullOrEmpty(filters["email"]))).ToList();
            }
        }
        public List<Client> MyClients
        {
            get
            {
                return MyOrders.Select(q => q.Client).ToList();
            }
        }

        public Client CurrentClient
        {
            get { return currentClient; }
            set
            {
                currentClient = value;
                OnPropertyChanged("CurrentClient");
            }
        }
        public UserControl ContentWindow
        {
            get { return content; }
            set
            {
                if (!fromStack)
                {
                    controls.Add(content);
                    OnPropertyChanged("BackElementVisibility");
                }
                content = value;
                fromStack = false;
                OnPropertyChanged("ContentWindow");
            }
        }
        public UserControl GetSpecific(Type type)
        {
            for (int i = 1; i <= controls.Count; i++)
            {
                var control = controls[controls.Count - i];
                if (control.GetType() == type)
                {
                    controls.RemoveAt(controls.Count - i);
                    return control;
                }
            }
            return controls.LastOrDefault();
        }
        public UserControl LastControl
        {
            get 
            {
                if (controls.Count != 0)
                {
                    fromStack = true;
                    var c = controls[controls.Count - 1];
                    controls.RemoveAt(controls.Count - 1);
                    OnPropertyChanged("BackElementVisibility");
                    return c;
                }
                else return ContentWindow;
            }
        }

        public DateTime? MinDate
        {
            get { return minTime; }
            set
            {
                minTime = value;
                OnPropertyChanged("MinDate");
                OnPropertyChanged("CurrentOrders");
            }
        }

        public DateTime? MaxDate
        {
            get { return maxTime; }
            set
            {
                maxTime = value;
                OnPropertyChanged("MaxDate");
                OnPropertyChanged("CurrentOrders");
            }
        }

        public bool OnlyMine
        {
            get { return onlyMine; }
            set
            {
                onlyMine = value;
                OnPropertyChanged("OnlyMine");
                OnPropertyChanged("CurrentOrders");
            }
        }

        public MainModel()
        {
            Controller.StaticPropertyChanged += OrdersData_StaticPropertyChanged;
        }

        void OrdersData_StaticPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("CurrentOrders");
        }

        public List<Order> CurrentOrders 
        {
            get { return OnlyMine ? 
                Controller.Orders.Where(q =>
                    q.Seller == Controller.CurrentUser && q.Id.ToString().Contains(NameSearch)).ToList() :
                Controller.Orders.Where(q => q.Id.ToString().Contains(NameSearch)).ToList(); }
        }
        public string NameSearch
        {
            get { return nameSearch; }
            set
            {
                nameSearch = value;
                OnPropertyChanged("NameSearch");
                OnPropertyChanged("CurrentOrders");
            }
        }
        public int? MinimalWorth
        {
            get { return minimalWorth; }
            set
            {
                minimalWorth = value;
                OnPropertyChanged("MinimalWorth");
                OnPropertyChanged("CurrentOrders");
            }
        }
        public int? MaximalWorth
        {
            get { return maximalWorth; }
            set
            {
                maximalWorth = value;
                OnPropertyChanged("MaximalWorth");
                OnPropertyChanged("CurrentOrders");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public void UpdateOrders()
        {

        }
        public MainModel Root;

    }
}
