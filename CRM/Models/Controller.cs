﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM
{
    static class Controller
    {
        public static Employee CurrentUser { get; set; }
        public static List<Order> Orders { get; set; }
        public static List<BuildingObject> Objects { get; set; }
        public static List<Client> Clients { get; set; }
        public static List<Employee> Employees { get; set; }
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void OnStaticPropertyChanged(string propertyName)
        {
            if (StaticPropertyChanged != null)
            {
                StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static Order GetOrder(long id)
        {
            return Orders.FirstOrDefault(q => q.Id == id);
        }

        public static T Get<T>(T obj)
        {
            if (obj.GetType() == typeof(Order))
                return (T)(dynamic)Orders.FirstOrDefault(q => q.Id == (obj as Order).Id);
            if (obj.GetType() == typeof(BuildingObject))
                return (T) (dynamic) Objects.FirstOrDefault(q => q.Id == (obj as BuildingObject).Id);
            if (obj.GetType() == typeof(Client))
                return (T)(dynamic)Clients.FirstOrDefault(q => q.Id == (obj as Client).Id);
            if (obj.GetType() == typeof(Employee))
                return (T)(dynamic)Employees.FirstOrDefault(q => q.Id == (obj as Employee).Id);
            return (T)(dynamic)null;
        }

        public static void Add(Order order)
        {
            Orders.Add(order);
            OnStaticPropertyChanged("Orders");
        }
        public static void Add(Client client)
        {
            Clients.Add(client);
            OnStaticPropertyChanged("Clients");
        }
        public static void Add(Employee employee)
        {
            Employees.Add(employee);
            OnStaticPropertyChanged("Employees");
        }
        public static void Add(BuildingObject buildingObject)
        {
            Objects.Add(buildingObject);
            OnStaticPropertyChanged("Objects");
        }

        public static void Remove(Order order)
        {
            Orders.Remove(order);
            OnStaticPropertyChanged("Orders");
        }
        public static void Remove(Client client)
        {
            Clients.Remove(client);
            OnStaticPropertyChanged("Clients");
        }
        public static void Remove(Employee employee)
        {
            Employees.Remove(employee);
            OnStaticPropertyChanged("Employees");
        }
        public static void Remove(BuildingObject buildingObject)
        {
            Objects.Remove(buildingObject);
            OnStaticPropertyChanged("Objects");
        }

        public static Order AddOrGet(Order order)
        {
            var dbOrder = Orders.FirstOrDefault(q => q == order);
            if (dbOrder != null)
                Orders.Add(order);
            return dbOrder;
        }
        public static Client AddOrGet(Client client)
        {
            var dbClient = Clients.FirstOrDefault(q => q.Name == client.Name);
            if (dbClient == null)
                Clients.Add(client);
            return dbClient;
        }
        public static Employee AddOrGet(Employee employee)
        {
            var dbEmployee = Employees.FirstOrDefault(q => q == employee);
            if (dbEmployee != null)
                Employees.Add(employee);
            return dbEmployee;
        }
        public static BuildingObject AddOrGet(BuildingObject obj)
        {
            var dbBuildingObject = Objects.FirstOrDefault(q => q == obj);
            if (dbBuildingObject != null)
                Objects.Add(obj);
            return dbBuildingObject;
        }

        public static void Change(Order order)
        {
            OnStaticPropertyChanged("Orders");
        }

        public static bool Auth(int id, string password)
        {
            var employee = Employees.FirstOrDefault(q => q.Id == id);
            if (employee != null && employee.Login(password))
            {
                CurrentUser = employee;
                return true;
            }
            return false;
        }

        static Controller()
        {
            Objects = new List<BuildingObject>();
            Clients = new List<Client>();
            Employees = new List<Employee>();
            Orders = new List<Order>();
            for (int i = 0; i < 20; i++)
            {
                var client = new Client { Name = "Клиент " + i, Id = i };
                var obj = new BuildingObject { Name = "Постройка " + i, Price = 100000 * (i + 1) / 5, Description = "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla " };
                var seller =  new Employee { Name = "Продавец " + i, Id = i };
                Objects.Add(obj);
                Clients.Add(client);
                Employees.Add(seller);
                Orders.Add(new Order { Client = client, CreationDate = DateTime.UtcNow.AddDays(-5 * i), FinishDate = DateTime.Today.AddDays(-i), Id = i, BuildingObject = obj, Seller = seller });
            }
            CurrentUser = Employees[0];
        }
    }
}
