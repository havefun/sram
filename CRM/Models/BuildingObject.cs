﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRM
{
    public enum BuildingObjectStatus
    {
        Продается,
        Зарезервировано,
        Продано
    }

    public enum BuildingType
    {
        Дача,
        Дом,
        Баня,
        Другое
    }
    public class BuildingObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public BuildingObjectStatus Status { get; set; }
        //public BuildingType Type { get; set; }
        public int Floors { get; set; }
        public int Rooms { get; set; }
        public double Square { get; set; }
        public string Address { get; set; }
        public DateTime BuiltYear { get; set; }
        public string Materials { get; set; }
        public bool Electricity { get; set; }
        public bool Sewers { get; set; }
        public bool WaterCommunications { get; set; }
        public bool Heating { get; set; }
        public bool Gas { get; set; }
        public double YardSquare { get; set; }
        public List<string> Photos { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }

        public BuildingObject()
        {
            Photos = new List<string>();
            for (int i = 0; i < 3; i++)
			{
                Photos.Add("");
			}
            Photos[0] = "http://www.domicomfort.ru/allimages/__8__75.jpg";
            Photos[1] = "http://www.domvderevne.ru/upload/medialibrary/80f/IMG_3910.JPG";
            Photos[2] = "http://furniturelab.ru/wp-content/uploads/2014/04/dizain-interera-doma_1.jpg";
        }
        public string FullDescription
        {
            get
            {
                StringBuilder sb = new StringBuilder("");
                sb.Append(string.Format("Этажность: {0}", Floors) + Environment.NewLine);
                sb.Append(string.Format("Количество комнат: {0}", Rooms) + Environment.NewLine);
                sb.Append(string.Format("Площадь: {0}", Square) + Environment.NewLine);
                sb.Append(string.Format("Адрес: {0}", Address) + Environment.NewLine);
                sb.Append(string.Format("Материалы: {0}", Materials) + Environment.NewLine);
                sb.Append(string.Format("Электричество: {0}", Electricity ? "Проведено" : "Отсутствует") + Environment.NewLine);
                sb.Append(string.Format("Канализация: {0}", Sewers ? "В начилии" : "-") + Environment.NewLine);
                sb.Append(string.Format("Водоснабжение: {0}", WaterCommunications ? "Общее" : "-") + Environment.NewLine);
                sb.Append(string.Format("Отопление: {0}", Heating ? "Присутствует" : "Отсутствует") + Environment.NewLine);
                sb.Append(string.Format("Газ: {0}", Gas ? "Проведен" : "Отсутствует") + Environment.NewLine);
                sb.Append(string.Format("Площадь участка: {0}", YardSquare) + Environment.NewLine);
                return sb.ToString();

            }
        }
    }
}