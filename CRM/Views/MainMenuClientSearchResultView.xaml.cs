﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CRM.Views
{
    /// <summary>
    /// Логика взаимодействия для MainMenuClientSearchResultView.xaml
    /// </summary>
    public partial class MainMenuClientSearchResultView : UserControl
    {
        public MainMenuClientSearchResultView()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as MainModel).ResetClientFilters();
            //(this.DataContext as OrderViewModel).ContentWindow = new Views.MainMenuOrderSelectionView();
        }
        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ClientsDataGrid.SelectedItem == null) return;
            var selectedClient = ClientsDataGrid.SelectedItem as Client;
            (DataContext as MainModel).CurrentClient = selectedClient;
            (this.DataContext as MainModel).ContentWindow = new Views.MainMenuOrderSelectionView();
        }
    }
}
