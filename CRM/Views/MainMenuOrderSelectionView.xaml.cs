﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CRM.Views
{
    /// <summary>
    /// Логика взаимодействия для MainMenuOrderSelectionView.xaml
    /// </summary>
    public partial class MainMenuOrderSelectionView : UserControl
    {
        public MainMenuOrderSelectionView()
        {
            InitializeComponent();
        }

        private void NewOrderButton_Click(object sender, RoutedEventArgs e)
        {
            var order = new Order() { Client = (this.DataContext as MainModel).CurrentClient };
            Controller.Add(order);
            (this.DataContext as MainModel).CurrentOrder = order;
            (this.DataContext as MainModel).ContentWindow = new Views.MainMenuOrderDetailsView();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
        }
        private void Orders_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (MyOrders.SelectedItem == null) return;
            var selectedOrder = MyOrders.SelectedItem as Order;

            (this.DataContext as MainModel).CurrentOrder = selectedOrder;
            (this.DataContext as MainModel).UpdateOrder();
            (this.DataContext as MainModel).ContentWindow = new Views.MainMenuOrderDetailsView();

        }
    }
}
