﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CRM.Views
{
    /// <summary>
    /// Логика взаимодействия для MainMenuOrderSearchResultsView.xaml
    /// </summary>
    public partial class MainMenuOrderSearchResultsView : UserControl
    {
        public MainMenuOrderSearchResultsView()
        {
            InitializeComponent();
        }

        private void Data_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void MyOrders_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (MyOrders.SelectedItem == null) return;
            var selectedOrder = MyOrders.SelectedItem as Order;

            (this.DataContext as MainModel).CurrentOrder = selectedOrder;
            (this.DataContext as MainModel).UpdateOrder();
            (this.DataContext as MainModel).ContentWindow = new Views.MainMenuOrderDetailsView();

        }
    }
}
