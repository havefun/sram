﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CRM.Views
{
    /// <summary>
    /// Логика взаимодействия для MainMenuOrderDetailsView.xaml
    /// </summary>
    public partial class MainMenuOrderDetailsView : UserControl
    {
        public MainMenuOrderDetailsView()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as MainModel).ContentWindow = new Views.MainMenuObjectsListView();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            var order = (this.DataContext as MainModel).CurrentOrder;
        }

        private void PrepareDocs_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as MainModel).CurrentOrder.Status = OrderStatus.Подтвержден;
            (this.DataContext as MainModel).UpdateOrder();
            MessageBox.Show("Документы распечатаны");
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as MainModel).CurrentOrder.Status = OrderStatus.Завершен;
            (this.DataContext as MainModel).UpdateOrder();
            MessageBox.Show("Операция завершена");
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as MainModel).CurrentOrder.Status = OrderStatus.Отклонен;
            (this.DataContext as MainModel).UpdateOrder();
            MessageBox.Show("Оплата отменена");
        }
    }
}
