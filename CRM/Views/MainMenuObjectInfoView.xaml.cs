﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CRM.Views
{
    /// <summary>
    /// Логика взаимодействия для MainMenuObjectsSearchView.xaml
    /// </summary>
    public partial class MainMenuObjectInfoView : UserControl
    {
        public MainMenuObjectInfoView()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var context = (this.DataContext as MainModel);
            context.CurrentOrder.BuildingObject = (this.DataContext as MainModel).CurrentObject;
            context.CurrentOrder.Status = OrderStatus.Осмотр;
            context.UpdateOrder();
            context.ContentWindow = context.GetSpecific(typeof(MainMenuOrderDetailsView));
        }
    }
}
