﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CRM.Views
{
    /// <summary>
    /// Логика взаимодействия для WindowDisplayContainer.xaml
    /// </summary>
    public partial class WindowDisplayContainer : Window
    {
        public WindowDisplayContainer(MainModel context, UserControl currentView, BuildingObject currentObject = null, Client currentClient = null, Order currentOrder = null)
        {
            InitializeComponent();
            DataContext = new MainModel() { Root = context, ContentWindow = currentView, CurrentClient = currentClient, CurrentObject = currentObject, CurrentOrder = currentOrder };

        }
        private void TextBlock_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            (this.DataContext as MainModel).ContentWindow = (this.DataContext as MainModel).LastControl;
        }
    }
}
