﻿using CRM.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CRM
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(Data.SelectedItem == null) return;
            var selectedOrder = Data.SelectedItem as Order;

            OrderView order = new OrderView();
            order.IsNew = false;
            order.DataContext = Controller.GetOrder(selectedOrder.Id);
            order.ShowDialog();
        }

        private void Orders_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (MyOrders.SelectedItem == null) return;
            var selectedOrder = MyOrders.SelectedItem as Order;

            (this.DataContext as MainModel).CurrentOrder = selectedOrder;
            (this.DataContext as MainModel).UpdateOrder();
            (this.DataContext as MainModel).ContentWindow = new Views.MainMenuOrderDetailsView();

        }

        private void MainClients_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(ClientsGrid.SelectedItem == null) return;
            var selectedOrder = ClientsGrid.SelectedItem as Client;

            (this.DataContext as MainModel).CurrentClient = selectedOrder;
            (this.DataContext as MainModel).UpdateOrder();
            (this.DataContext as MainModel).ContentWindow = new Views.MainMenuOrderSelectionView();
            
        }
        private void Object_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ClientsDataGrid.SelectedItem == null) return;
            var selected = ClientsDataGrid.SelectedItem as BuildingObject;
            new WindowDisplayContainer((this.DataContext as MainModel), new EditObjectView(), currentObject: selected ).Show();
            //OrderView order = new OrderView();
            //order.IsNew = false;
            //order.DataContext = Controller.GetOrder(selectedOrder.Id);
            //order.ShowDialog();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Data.Items.Refresh();
            OrderView order = new OrderView();
            order.IsNew = true;
            order.DataContext = new Order { Id = Controller.Orders.Count, Client = new Client(), CreationDate = DateTime.Now, BuildingObject = new BuildingObject(), Seller = new Employee() };
            order.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            //Data.ItemsSource = (this.DataContext as OrderViewModel).CurrentOrders;
        }

        private void TextBlock_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            (this.DataContext as MainModel).ContentWindow = (this.DataContext as MainModel).LastControl;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as MainModel).ResetClientFilters();
        }

    }
}
