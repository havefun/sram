﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CRM.Views
{
    /// <summary>
    /// Логика взаимодействия для MainMenuObjectsListView.xaml
    /// </summary>
    public partial class MainMenuObjectsListView : UserControl
    {
        public MainMenuObjectsListView()
        {
            InitializeComponent();
        }
        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ClientsDataGrid.SelectedItem == null) return;
            var obj = ClientsDataGrid.SelectedItem as BuildingObject;

            (this.DataContext as MainModel).CurrentObject = obj;
            (this.DataContext as MainModel).ContentWindow = new MainMenuObjectInfoView();
        }
    }
}
